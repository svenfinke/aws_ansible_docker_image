FROM ubuntu

# Install Ansible
RUN apt update && \
    apt install -y software-properties-common && \
    apt-add-repository --yes --update ppa:ansible/ansible
RUN apt install -y ansible

# Install AWS CLI
RUN apt install -y curl unzip && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Install AWS CLI module deps
RUN apt install -y python-pip && \
    pip install --user botocore boto3

WORKDIR /ansible